/**
 * \file
 * \copyright
 * Copyright (c) 2012-2022, OpenGeoSys Community (http://www.opengeosys.org)
 *            Distributed under a Modified BSD License.
 *              See accompanying file LICENSE.txt or
 *              http://www.opengeosys.org/project/license
 *
 * Created on September 09, 2022, 10:08 AM
 */

#include "CreatePlaneWeaknessPermeability.h"

#include <string>

#include "BaseLib/ConfigTree.h"
#include "BaseLib/Error.h"
#include "Parameter.h"
#include "ParameterLib/CoordinateSystem.h"
#include "ParameterLib/Parameter.h"
#include "ParameterLib/Utils.h"
#include "PlaneWeaknessPermeability.h"

namespace MaterialPropertyLib
{
std::unique_ptr<Property> createPlaneWeaknessPermeability(
    int const geometry_dimension,
    BaseLib::ConfigTree const& config,
    std::vector<std::unique_ptr<ParameterLib::ParameterBase>> const& parameters,
    ParameterLib::CoordinateSystem const* const local_coordinate_system)
{
    if ((geometry_dimension != 2) && (geometry_dimension != 3))
    {
        OGS_FATAL(
            "The PlaneWeaknessPermeability is implemented only for 2D or 3D "
            "problems");
    }

    //! \ogs_file_param{properties__property__type}
    config.checkConfigParameter("type", "PlaneWeaknessPermeability");

    // Second access for storage.
    //! \ogs_file_param{properties__property__name}
    auto property_name = config.peekConfigParameter<std::string>("name");

    DBUG("Create PlaneWeaknessPermeability property {:s}.", property_name);

    std::string const& parameter_name =
        //! \ogs_file_param{properties__property__PlaneWeaknessPermeability__initial_permeability}
        config.getConfigParameter<std::string>("initial_permeability");
    auto const& parameter_k0 = ParameterLib::findParameter<double>(
        parameter_name, parameters, 0, nullptr);

    auto const b1 =
        //! \ogs_file_param{properties__property__PlaneWeaknessPermeability__b1}
        config.getConfigParameter<double>("b1");
    auto const b2 =
        //! \ogs_file_param{properties__property__PlaneWeaknessPermeability__b2}
        config.getConfigParameter<double>("b2");
    auto const b3 =
        //! \ogs_file_param{properties__property__PlaneWeaknessPermeability__b3}
        config.getConfigParameter<double>("b3");
    auto const xp =
        //! \ogs_file_param{properties__property__PlaneWeaknessPermeability__b1}
        config.getConfigParameter<double>("x_normal");
    auto const yp =
        //! \ogs_file_param{properties__property__PlaneWeaknessPermeability__b2}
        config.getConfigParameter<double>("y_normal");
    auto const zp =
        //! \ogs_file_param{properties__property__PlaneWeaknessPermeability__b3}
        config.getConfigParameter<double>("z_normal");
    auto const thickness =
        //! \ogs_file_param{properties__property__PlaneWeaknessPermeability__b3}
        config.getConfigParameter<double>("thickness");
    auto const minimum_permeability =
        //! \ogs_file_param{properties__property__PlaneWeaknessPermeability__minimum_permeability}
        config.getConfigParameter<double>("minimum_permeability");
    auto const maximum_permeability =
        //! \ogs_file_param{properties__property__PlaneWeaknessPermeability__maximum_permeability}
        config.getConfigParameter<double>("maximum_permeability");

    if (minimum_permeability > maximum_permeability)
    {
        OGS_FATAL(
            "The value of minimum_permeability of {:e} is larger that the "
            "value of maximum_permeability of {:e} in "
            "PlaneWeaknessPermeability",
            minimum_permeability, maximum_permeability);
    }

    if (geometry_dimension == 2)
    {
        return std::make_unique<PlaneWeaknessPermeability<2>>(
            std::move(property_name), parameter_k0, xp,yp,zp,b1, b2, b3, thickness,
            minimum_permeability, maximum_permeability,
            local_coordinate_system);
    }

    return std::make_unique<PlaneWeaknessPermeability<3>>(
        std::move(property_name), parameter_k0, xp,yp,zp, b1, b2, b3, thickness,
        minimum_permeability, maximum_permeability, local_coordinate_system);
}
}  // namespace MaterialPropertyLib

/**
 * \file
 * \copyright
 * Copyright (c) 2012-2022, OpenGeoSys Community (http://www.opengeosys.org)
 *            Distributed under a Modified BSD License.
 *              See accompanying file LICENSE.txt or
 *              http://www.opengeosys.org/project/license
 *
 * Created on September 09, 2022, 8:49 AM
 */

#include "PlaneWeaknessPermeability.h"

#include <cmath>
#include <limits>

#include "BaseLib/Error.h"
#include "MaterialLib/MPL/Medium.h"
#include "MathLib/MathTools.h"
#include "ParameterLib/CoordinateSystem.h"
#include "ParameterLib/Parameter.h"

#include <iostream>
namespace MaterialPropertyLib
{
template <int DisplacementDim>
PlaneWeaknessPermeability<DisplacementDim>::PlaneWeaknessPermeability(
    std::string name, ParameterLib::Parameter<double> const& k0,
    double const xp, double const yp, double const zp,
    double const b1, double const b2, double const b3,double const thickness,
    double const minimum_permeability, double const maximum_permeability,
    ParameterLib::CoordinateSystem const* const local_coordinate_system)
    : k0_(k0),
      b1_(b1),
      b2_(b2),
      b3_(b3),
      thickness_(thickness),
      xp_(xp),
      yp_(yp),
      zp_(zp),
      minimum_permeability_(minimum_permeability),
      maximum_permeability_(maximum_permeability),
      local_coordinate_system_(local_coordinate_system)
{
    name_ = std::move(name);
}

template <int DisplacementDim>
void PlaneWeaknessPermeability<DisplacementDim>::checkScale() const
{
    if (!std::holds_alternative<Medium*>(scale_))
    {
        OGS_FATAL(
            "The property 'PlaneWeaknessPermeability' is "
            "implemented on the 'medium' scale only.");
    }
}
// template <int DisplacementDim>
// typename PlaneWeaknessPermeability<DisplacementDim>::KelvinVector sigma_rotation(
    // double const nx, double const ny, double const nz,
    // typename PlaneWeaknessPermeability<DisplacementDim>::KelvinVector const& sigma_try)
// {
    // static int const KelvinVectorSize =
        // MathLib::KelvinVector::kelvin_vector_dimensions(DisplacementDim);
		
    
    // // std::cout << "Compute rotation matrix \n";
    // // given the normal nx ny nz, the rotation is obtained as  
	// // sigma_matrix_rot = A sigma_matrix A_transposed 
	// // where A is obtained from the normal nx ny nz of the weakness plane
    // // std::cout << "the normal vector is\n" << nx << " " << ny  <<" "<< nz<<"\n";
    
	
    // Eigen::Matrix<double, 3, 3> A_rot;
    // Eigen::Matrix<double, 3, 3> A_rot_t;
    // if (nz==1)
	// {
     // A_rot << 0,0,0,0,0,0,0,0,1;
	// }
	// else
	// {
	 // double nzsq=std::sqrt(1-nz*nz);
     // A_rot << -ny/nzsq, -nx/nzsq, 0.0, -nx*nz/nzsq,ny*nz/nzsq, -nzsq,nx,-ny,-nz;
	// }
	// A_rot_t=A_rot.transpose();
	// // build matrix for computations
	// Eigen::Matrix<double, 3, 3> sigma_matrix;
	// Eigen::Matrix<double, 3, 3> sigma_matrix_rot;
	// Eigen::Matrix<double, 3, 3> sigma_new;
	// //Eigen::Matrix<double, 3, 3> sigma_correct_back_rotated;
     // sigma_matrix << sigma_try[0], sigma_try[3], sigma_try[5], 
				     // sigma_try[3], sigma_try[1], sigma_try[4], 
				     // sigma_try[5], sigma_try[4], sigma_try[2];
		 
    // sigma_matrix_rot=A_rot*sigma_matrix*A_rot_t;
	// // OGS:    11 22 33 12 23 13
    // // OGS_plane of weakness:    1'1' 2'2' norm 1'2' 2'3' 1'3'


	// return sigma_matrix_rot.coeff(2,2);
// }
template <int DisplacementDim>
PropertyDataType PlaneWeaknessPermeability<DisplacementDim>::value(
    VariableArray const& variable_array,
    ParameterLib::SpatialPosition const& pos, double const t,
    double const /*dt*/) const
{
    auto const& stress_vector =
        std::get<SymmetricTensor<DisplacementDim>>(variable_array.total_stress);
    //double const e_vol = variable_array.volumetric_strain;
    //double const e_vol_pls = variable_array.equivalent_plastic_strain;
    double const gas_pressure = variable_array.phase_pressure;
	double const norm_stress_r = variable_array.normal_stress_at_rupture;
	double const shear_stress_r = variable_array.shear_stress_cumulative;
    //std::cout << "The shear_stress_r (cumulative shear) is " << shear_stress_r;
    auto k_data = k0_(t, pos);
    //std::cout << "The permeability k_i was ";
    //for (auto& k_i : k_data)
    //{        std::cout << " " << k_i ;
	//}
	double br,bstress,bshear;
    double nx_=xp_;
    double ny_=yp_;
    double nz_=zp_;
    //std::cout << "nx " << nx_ << "ny " << ny_ << "nz " << nz_ <<"\n";
    //auto k_data = k0_(t, pos);
    
    
    //std::cout << "Read vars array? \n normal_stress_at_rupture " << norm_stress_r <<"\n";
    
    if (norm_stress_r > 0.0) 
	{
     Eigen::Matrix<double, 3, 3> A_rot;
     Eigen::Matrix<double, 3, 3> A_rot_t;
     if (nz_ ==1)
     {
      A_rot << 0,0,0,0,0,0,0,0,1;
     }
     else
     {
      double nzsq=std::sqrt(1-nz_*nz_);
      A_rot << -ny_/nzsq, -nx_/nzsq, 0.0, -nx_*nz_/nzsq,ny_*nz_/nzsq, -nzsq,nx_,-ny_,-nz_;
     }
     A_rot_t=A_rot.transpose();
     //std::cout << "A_rot " << A_rot_t<<"\n"; 
     //std::cout << "A_rot_t " << A_rot <<"\n"; 
     // build matrix for computations
     Eigen::Matrix<double, 3, 3> sigma_matrix;
     Eigen::Matrix<double, 3, 3> sigma_matrix_rot;
     Eigen::Matrix<double, 3, 3> sigma_new;
     //Eigen::Matrix<double, 3, 3> sigma_correct_back_rotated;
      sigma_matrix << stress_vector[0], stress_vector[3], stress_vector[5], 
     			     stress_vector[3], stress_vector[1], stress_vector[4], 
     			     stress_vector[5], stress_vector[4], stress_vector[2];
     //std::cout << "stress " << sigma_matrix <<"\n"; 
     sigma_matrix_rot=A_rot*sigma_matrix*A_rot_t;
     //std::cout << "stress " << sigma_matrix_rot <<"\n"; 
     
     double const norm_stress_now=-sigma_matrix_rot.coeff(2,2);
     
     //std::cout << "Read vars array in perm? \n cumulative shear " << shear_stress_r << "normal_stress_at_rupture " << norm_stress_r <<", current normal stress "<< norm_stress_now<<"\n";
     
     if (b1_ > 0 && norm_stress_r > 0.0 ) // constant aperture after rupture 
     {
      
     br=b1_;
     }
     else
     {
     br=0;
     }
     if (b2_ > 0 && norm_stress_r > 0.0 ) // elastic aperture of the fracture walls after rupture
     {
	 	
	 	bstress=1/b2_* std::max(0.0,(-norm_stress_now+norm_stress_r));
	 	if (norm_stress_now < 0)
	 	{
	 		bstress = 250.e-6;
	 	}
     }
     else
     {
     bstress=0;
     }
     if (b1_ < 0  ) // elastic aperture of the fracture walls  NOT COUPLED TO M
     {
	 	
	 	bstress=std::clamp(1/b2_* std::max(0.0,(gas_pressure - 0.5e6)),0.0,500e-6);
	 	if (norm_stress_now < 0)
	 	{
	 		bstress = 500.e-6;
	 	}
     }
     else
     {
     bstress=0;
     }
	 if (b3_ > 0 && norm_stress_r > 0.0 ) // dilation after rupture 
     {
     
     bshear= std::clamp(1/b2_*shear_stress_r*std::tan(b3_/180.0 *  3.141592653589), 0.e-6, 250.e-6);//
     }
     else
     {
     bshear=0;
     }
	  
	 //std::cout << "\n" ;   
	 //std::cout << "The permeability k_i is due to br,bshear,bstress" << br <<" " << bshear <<" "<< bstress << "and is ";
     for (auto& k_i : k_data)
     {        k_i = std::clamp((br + bshear + bstress) * (br + bshear + bstress) *
                           (br + bshear + bstress) / (12 * thickness_) , minimum_permeability_,maximum_permeability_);
	 		  //std::cout << " " << k_i ;
	 }
	 //std::cout << "\n" ;
     
     
     //double const ten_base_exponent = e_vol > 0.0 ? b3_ * e_vol : b2_ * e_vol;
     //double const factor =
       //  std::pow(10.0, ten_base_exponent) * std::exp(b1_ * e_vol_pls);
	 
     // for (auto& k_i : k_data)
     // {
         // k_i = std::clamp(k_i * 0 + factor, minimum_permeability_,
                          // maximum_permeability_);
     // }
	 
     // Local coordinate transformation is only applied for the case that the
     // initial intrinsic permeability is given with orthotropic assumption.
    }
	if (local_coordinate_system_ && (k_data.size() == DisplacementDim))
    {
        Eigen::Matrix<double, DisplacementDim, DisplacementDim> const e =
            local_coordinate_system_->transformation<DisplacementDim>(pos);
        Eigen::Matrix<double, DisplacementDim, DisplacementDim> k =
            Eigen::Matrix<double, DisplacementDim, DisplacementDim>::Zero();

        for (int i = 0; i < DisplacementDim; ++i)
        {
            Eigen::Matrix<double, DisplacementDim, DisplacementDim> const
                ei_otimes_ei = e.col(i) * e.col(i).transpose();

            k += k_data[i] * ei_otimes_ei;
        }
        return k;
    }

    return fromVector(k_data);
}

template <int DisplacementDim>
PropertyDataType PlaneWeaknessPermeability<DisplacementDim>::dValue(
    VariableArray const& /*variable_array*/, Variable const variable,
    ParameterLib::SpatialPosition const& /*pos*/, double const /*t*/,
    double const /*dt*/) const
{
    if (variable == Variable::mechanical_strain)
    {
        return 0.;
    }

    OGS_FATAL(
        "The derivative of the intrinsic permeability of "
        "PlaneWeaknessPermeabilityis not implemented.");
}
template class PlaneWeaknessPermeability<2>;
template class PlaneWeaknessPermeability<3>;
}  // namespace MaterialPropertyLib

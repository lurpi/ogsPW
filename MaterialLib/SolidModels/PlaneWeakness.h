/**
 * \file
 * \author Luca Urpi
 * \date   Sep 7, 2022
 *
 * \copyright
 * Copyright (c) 2012-2022, OpenGeoSys Community (http://www.opengeosys.org)
 *            Distributed under a Modified BSD License.
 *              See accompanying file LICENSE.txt or
 *              http://www.opengeosys.org/project/license
 *
 *
 * Implementation of Urpi's OGS5 plane of weakness model.
 * see Urpi's paper "Hydro-mechanical fault reactivation modeling based on
 * elasto-plasticity with embedded weakness planes" for more details. \cite Urpi2020
 * 
 * OGS's Kelvin vector indices
 *               OGS:    11 22 33 12 23 13
 * Refer to 
 * for more details for the tests.
 */

#pragma once

#ifndef NDEBUG
#include <ostream>
#endif

#include <cmath>
#include <memory>
#include <tuple>

#include "BaseLib/Error.h"
#include "MathLib/KelvinVector.h"
#include "NumLib/NewtonRaphson.h"
#include "ParameterLib/Parameter.h"

#include "MechanicsBase.h"

namespace MaterialLib
{
namespace Solids
{
namespace PlaneWeakness
{
enum class TangentType
{
    Elastic,
    PlasticDamageSecant,
    Plastic
};
inline TangentType makeTangentType(std::string const& s)
{
    if (s == "Elastic")
    {
        return TangentType::Elastic;
    }
    if (s == "PlasticDamageSecant")
    {
        return TangentType::PlasticDamageSecant;
    }
    if (s == "Plastic")
    {
        return TangentType::Plastic;
    }
    OGS_FATAL("Not valid string '{:s}' to create a tangent type from.", s);
}

/// material parameters in relation to \cite Urpi2020.
struct MaterialPropertiesParameters
{
    using P = ParameterLib::Parameter<double>;

    MaterialPropertiesParameters(P const& G_, P const& K_,
                                 P const& Cohesion_,
                                 P const& FrictionAngle_, 
                                 P const& DilationAngle_,
                                 P const& TensionCutOffParam_,
                                 P const& x_normal_, P const& y_normal_, P const& z_normal_)
        : G(G_),
          K(K_),
          Cohesion(Cohesion_),
          FrictionAngle(FrictionAngle_),
          DilationAngle(DilationAngle_),
          TensionCutOffParameter(TensionCutOffParam_),
          x_normal(x_normal_),
          y_normal(y_normal_),
          z_normal(z_normal_)
    {
    }
    // basic material parameters
    P const& G;  ///< shear modulus
    P const& K;  ///< bulk modulus

    P const& Cohesion;     ///< \copydoc Plane of Weakness Cohesion 
    P const& FrictionAngle;             ///< \copydoc Plane of Weakness friction angle
    P const& DilationAngle;            ///< \copydoc Plane of Weakness dilation angle
    P const& TensionCutOffParameter;    ///< \copydoc Plane of Weakness tensio cut-off
    P const& x_normal;    ///< \copydoc component x of the normal to the plane of weakness
    P const& y_normal;    ///< \copydoc component y of the normal to the plane of weakness
    P const& z_normal;    ///< \copydoc component z of the normal to the plane of weakness

};

struct DamagePropertiesParameters
{
    using P = ParameterLib::Parameter<double>;
    P const& TensionCutOffParameter_d;
    P const& FrictionAngle_d;
    P const& h_d;
};

/// Evaluated MaterialPropertiesParameters container, see its documentation for
/// details.
struct MaterialProperties final
{
    MaterialProperties(double const t, ParameterLib::SpatialPosition const& x,
                       MaterialPropertiesParameters const& mp)
        : G(mp.G(t, x)[0]),
          K(mp.K(t, x)[0]),
          Cohesion(mp.Cohesion(t, x)[0]),
          TensionCutOffParameter(mp.TensionCutOffParameter(t, x)[0]),
          FrictionAngle(mp.FrictionAngle(t, x)[0]),
          DilationAngle(mp.DilationAngle(t, x)[0]),
          x_normal(mp.x_normal(t, x)[0]),
          y_normal(mp.y_normal(t, x)[0]),
          z_normal(mp.z_normal(t, x)[0])
    {
    }
    double const G;
    double const K;

    double const Cohesion;
    double const TensionCutOffParameter;
    double const FrictionAngle;
    double const DilationAngle;

    double const x_normal;
    double const y_normal;
    double const z_normal;

};

/// Evaluated DamagePropertiesParameters container, see its documentation for
/// details.
struct DamageProperties
{
    DamageProperties(double const t,
                     ParameterLib::SpatialPosition const& x,
                     DamagePropertiesParameters const& dp)
        : TensionCutOffParameter_d(dp.TensionCutOffParameter_d(t, x)[0]),
          FrictionAngle_d(dp.FrictionAngle_d(t, x)[0]),
          h_d(dp.h_d(t, x)[0])
    {
    }
    double const TensionCutOffParameter_d;
    double const FrictionAngle_d;
    double const h_d;
};

template <typename KelvinVector>
struct PlasticStrain final
{
    PlasticStrain() : D(KelvinVector::Zero()) {}
    PlasticStrain(KelvinVector eps_p_D_, double const eps_p_V_,
                  double const eps_p_eff_)
        : D(std::move(eps_p_D_)), V(eps_p_V_), eff(eps_p_eff_){};

    KelvinVector D;  ///< deviatoric plastic strain
    double V = 0;    ///< volumetric strain
    double eff = 0;  ///< effective plastic strain

    EIGEN_MAKE_ALIGNED_OPERATOR_NEW;
};

class Damage final
{
public:
    Damage() = default;
    Damage(double const cohesion_d, double const value)
        : _cohesion_d(cohesion_d), _value(value)
    {
    }

    double cohesion_d() const { return _cohesion_d; }
    double& cohesion_d() { return _cohesion_d; }
    double value() const { return _value; }
    double& value() { return _value; }

private:
    double _cohesion_d = 0;  ///< damage driving variable
    double _value = 0;    ///< isotropic damage variable
};

template <int DisplacementDim>
struct StateVariables
    : public MechanicsBase<DisplacementDim>::MaterialStateVariables
{
    void setInitialConditions()
    {
        eps_p = eps_p_prev;
        rupt_flag = rupt_flag_prev;
        shear_sum = shear_sum_prev;
        damage = damage_prev;
    }

    void pushBackState() override
    {
        eps_p_prev = eps_p;
        rupt_flag_prev = rupt_flag;
        shear_sum_prev = shear_sum;
        damage_prev = damage;
    }

    double getEquivalentPlasticStrain() const override;
    double getRuptureHappened() const override;
    double getShearCumulat() const override;

    static int const KelvinVectorSize =
        MathLib::KelvinVector::kelvin_vector_dimensions(DisplacementDim);
    using Invariants = MathLib::KelvinVector::Invariants<KelvinVectorSize>;
    using KelvinVector =
        MathLib::KelvinVector::KelvinVectorType<DisplacementDim>;

    PlasticStrain<KelvinVector> eps_p;  ///< plastic part of the state.
    
    double rupt_flag;
    double shear_sum;  
    Damage damage;                      ///< damage part of the state.

    // Initial values from previous timestep
    PlasticStrain<KelvinVector> eps_p_prev;  ///< \copydoc eps_p
    double rupt_flag_prev;
    double shear_sum_prev;
    Damage damage_prev;                      ///< \copydoc damage

#ifndef NDEBUG
    friend std::ostream& operator<<(
        std::ostream& os, StateVariables<DisplacementDim> const& m)
    {
        os << "State:\n"
           << "eps_p_D: " << m.eps_p.D << "\n"
           << "eps_p_eff: " << m.eps_p.eff << "\n"
           << "cohesion_d: " << m.damage.cohesion_d() << "\n"
           << "damage: " << m.damage.value() << "\n"
           << "eps_p_D_prev: " << m.eps_p_prev.D << "\n"
           << "eps_p_eff_prev: " << m.eps_p_prev.eff << "\n"
           << "cohesion_d_prev: " << m.damage_prev.cohesion_d() << "\n"
           << "damage_prev: " << m.damage_prev.value() << "\n"
           << "lambda: " << m.lambda << "\n";
        return os;
    }
#endif  // NDEBUG

    EIGEN_MAKE_ALIGNED_OPERATOR_NEW;
};

template <int DisplacementDim>
class PlaneWeakness final : public MechanicsBase<DisplacementDim>
{
public:
    static int const KelvinVectorSize =
        MathLib::KelvinVector::kelvin_vector_dimensions(DisplacementDim);
    static int const JacobianResidualSize =
        2 * KelvinVectorSize + 3;  // 2 is the number of components in the
                                   // jacobian/residual, not the space
                                   // dimension. And 3 is for additional
                                   // variables.
    using ResidualVectorType = Eigen::Matrix<double, KelvinVectorSize, 1>;
    using JacobianMatrix = Eigen::Matrix<double, KelvinVectorSize,
                                         KelvinVectorSize, Eigen::RowMajor>;

public:
    std::unique_ptr<
        typename MechanicsBase<DisplacementDim>::MaterialStateVariables>
    createMaterialStateVariables() const override
    {
        return std::make_unique<StateVariables<DisplacementDim>>();
    }

public:
    using KelvinVector =
        MathLib::KelvinVector::KelvinVectorType<DisplacementDim>;
    using KelvinMatrix =
        MathLib::KelvinVector::KelvinMatrixType<DisplacementDim>;

public:
    PlaneWeakness(
        NumLib::NewtonRaphsonSolverParameters nonlinear_solver_parameters,
        MaterialPropertiesParameters material_properties,
        std::unique_ptr<DamagePropertiesParameters>&& damage_properties,
        TangentType tangent_type);

    double computeFreeEnergyDensity(
        double const /*t*/,
        ParameterLib::SpatialPosition const& /*x*/,
        double const /*dt*/,
        KelvinVector const& epsilon,
        KelvinVector const& sigma,
        typename MechanicsBase<DisplacementDim>::MaterialStateVariables const&
            material_state_variables) const override;

    std::optional<std::tuple<KelvinVector,
                             std::unique_ptr<typename MechanicsBase<
                                 DisplacementDim>::MaterialStateVariables>,
                             KelvinMatrix>>
    integrateStress(
        MaterialPropertyLib::VariableArray const& variable_array_prev,
        MaterialPropertyLib::VariableArray const& variable_array,
        double const t, ParameterLib::SpatialPosition const& x, double const dt,
        typename MechanicsBase<DisplacementDim>::MaterialStateVariables const&
            material_state_variables) const override;

    std::vector<typename MechanicsBase<DisplacementDim>::InternalVariable>
    getInternalVariables() const override;

    MaterialProperties evaluatedMaterialProperties(
        double const t, ParameterLib::SpatialPosition const& x) const
    {
        return MaterialProperties(t, x, _mp);
    }

    double getBulkModulus(double const t,
                          ParameterLib::SpatialPosition const& x,
                          KelvinMatrix const* const /*C*/) const override
    {
        return _mp.K(t, x)[0];
    }

    DamageProperties evaluatedDamageProperties(
        double const t, ParameterLib::SpatialPosition const& x) const
    {
        return DamageProperties(t, x, *_damage_properties);
    }

private:
    NumLib::NewtonRaphsonSolverParameters const _nonlinear_solver_parameters;

    MaterialPropertiesParameters _mp;
    std::unique_ptr<DamagePropertiesParameters> _damage_properties;
    TangentType const _tangent_type;
};

extern template class PlaneWeakness<2>;
extern template class PlaneWeakness<3>;

extern template struct StateVariables<2>;
extern template struct StateVariables<3>;
}  // namespace PlaneWeakness
}  // namespace Solids
}  // namespace MaterialLib

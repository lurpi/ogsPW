/**
 * \file
 * \author Luca Urpi
 * \date   Sep 7, 2022
 *
 * \copyright
 * Copyright (c) 2012-2022, OpenGeoSys Community (http://www.opengeosys.org)
 *            Distributed under a Modified BSD License.
 *              See accompanying file LICENSE.txt or
 *              http://www.opengeosys.org/project/license
 *
 * OGS's Kelvin vector indices
 *               OGS:    11 22 33 12 23 13
 */

#pragma once

#include "PlaneWeakness.h"
#include "NumLib/CreateNewtonRaphsonSolverParameters.h"
#include "ParameterLib/Utils.h"

namespace MaterialLib
{
namespace Solids
{
namespace PlaneWeakness
{
inline std::unique_ptr<DamagePropertiesParameters> createDamageProperties(
    std::vector<std::unique_ptr<ParameterLib::ParameterBase>> const& parameters,
    BaseLib::ConfigTree const& config)
{
    //! \ogs_file_param_special{material__solid__constitutive_relation__PlaneWeakness__damage_properties__tensioncutoffparameter_d}
    auto& tensioncutoffparameter_d =
        ParameterLib::findParameter<double>(config, "tensioncutoffparameter_d", parameters, 1);

    DBUG("Use '{:s}' as tensioncutoffparameter_d.", tensioncutoffparameter_d.name);

    //! \ogs_file_param_special{material__solid__constitutive_relation__PlaneWeakness__damage_properties__frictionangle_d}
    auto& frictionangle_d =
        ParameterLib::findParameter<double>(config, "frictionangle_d", parameters, 1);

    DBUG("Use '{:s}' as frictionangle_d.", frictionangle_d.name);

    //! \ogs_file_param_special{material__solid__constitutive_relation__PlaneWeakness__damage_properties__h_d}
    auto& h_d =
        ParameterLib::findParameter<double>(config, "h_d", parameters, 1);

    DBUG("Use '{:s}' as h_d (threshold value for weakening).", h_d.name);

    return std::make_unique<DamagePropertiesParameters>(
        DamagePropertiesParameters{tensioncutoffparameter_d, frictionangle_d, h_d});
}

template <int DisplacementDim>
std::unique_ptr<PlaneWeakness<DisplacementDim>> createPlaneWeakness(
    std::vector<std::unique_ptr<ParameterLib::ParameterBase>> const& parameters,
    BaseLib::ConfigTree const& config)
{
    //! \ogs_file_param{material__solid__constitutive_relation__type}
    config.checkConfigParameter("type", "PlaneWeakness");
    DBUG("Create PlaneWeakness material");

    //! \ogs_file_param_special{material__solid__constitutive_relation__PlaneWeakness__shear_modulus}
    auto& shear_modulus = ParameterLib::findParameter<double>(
        config, "shear_modulus", parameters, 1);

    DBUG("Use '{:s}' as shear modulus parameter.", shear_modulus.name);

    //! \ogs_file_param_special{material__solid__constitutive_relation__PlaneWeakness__bulk_modulus}
    auto& bulk_modulus = ParameterLib::findParameter<double>(
        config, "bulk_modulus", parameters, 1);

    DBUG("Use '{:s}' as bulk modulus parameter.", bulk_modulus.name);

    //! \ogs_file_param_special{material__solid__constitutive_relation__PlaneWeakness__cohesion}
    auto& cohesion_weakplane =
        ParameterLib::findParameter<double>(config, "cohesion_weakplane", parameters, 1);

    DBUG("Use '{:s}' as cohesion of the weak plane.", cohesion_weakplane.name);

    //! \ogs_file_param_special{material__solid__constitutive_relation__PlaneWeakness__frictionangle}
    auto& frictionangle_weakplane =
        ParameterLib::findParameter<double>(config, "frictionangle_weakplane", parameters, 1);

    DBUG("Use '{:s}' as friction angle (in degree) of the weakplane.", frictionangle_weakplane.name);

    //! \ogs_file_param_special{material__solid__constitutive_relation__PlaneWeakness__dilationangle}
    auto& dilationangle_weakplane =
        ParameterLib::findParameter<double>(config, "dilationangle_weakplane", parameters, 1);

    DBUG("Use '{:s}' as dilationangle of the weakplane.", dilationangle_weakplane.name);

    //! \ogs_file_param_special{material__solid__constitutive_relation__PlaneWeakness__tensioncutoffparameter}
    auto& tensioncutoffparameter_weakplane =
        ParameterLib::findParameter<double>(config, "tensioncutoffparameter_weakplane", parameters, 1);

    DBUG("Use '{:s}' as tension cut-off parameter of the weak plane.", tensioncutoffparameter_weakplane.name);

    //! \ogs_file_param_special{material__solid__constitutive_relation__PlaneWeakness__x_normal_weakplane}
    auto& x_normal_weakplane =
        ParameterLib::findParameter<double>(config, "x_normal_weakplane", parameters, 1);

    DBUG("Use '{:s}' as x_normal of the weakplane (cos of the angle between normal of the weak plane and x-axis).", x_normal_weakplane.name);

    //! \ogs_file_param_special{material__solid__constitutive_relation__PlaneWeakness__y_normal_weakplane}
    auto& y_normal_weakplane =
        ParameterLib::findParameter<double>(config, "y_normal_weakplane", parameters, 1);

    DBUG("Use '{:s}' as y_normal of the weakplane (cos of the angle between normal of the weak plane and y-axis).", y_normal_weakplane.name);

    //! \ogs_file_param_special{material__solid__constitutive_relation__PlaneWeakness__z_normal_weakplane}
    auto& z_normal_weakplane = ParameterLib::findParameter<double>(config, "z_normal_weakplane", parameters, 1);

    DBUG("Use '{:s}' as z_normal of the weakplane (cos of the angle between normal of the weak plane and z-axis).", z_normal_weakplane.name);

    auto tangent_type =
        //! \ogs_file_param{material__solid__constitutive_relation__PlaneWeakness__tangent_type}
        makeTangentType(config.getConfigParameter<std::string>("tangent_type"));

    MaterialPropertiesParameters mp{
        shear_modulus, bulk_modulus, cohesion_weakplane, frictionangle_weakplane,
        dilationangle_weakplane, tensioncutoffparameter_weakplane, 
        x_normal_weakplane, y_normal_weakplane, z_normal_weakplane };

    // Damage properties.
    std::unique_ptr<DamagePropertiesParameters> PlaneWeakness_damage_properties;

    auto const& PlaneWeakness_damage_config =
        //! \ogs_file_param{material__solid__constitutive_relation__PlaneWeakness__damage_properties}
        config.getConfigSubtreeOptional("damage_properties");
    if (PlaneWeakness_damage_config)
    {
        PlaneWeakness_damage_properties =
            createDamageProperties(parameters, *PlaneWeakness_damage_config);
    }

    auto const& nonlinear_solver_config =
        //! \ogs_file_param{material__solid__constitutive_relation__PlaneWeakness__nonlinear_solver}
        config.getConfigSubtree("nonlinear_solver");
    auto const nonlinear_solver_parameters =
        NumLib::createNewtonRaphsonSolverParameters(nonlinear_solver_config);

    return std::make_unique<PlaneWeakness<DisplacementDim>>(
        nonlinear_solver_parameters,
        mp,
        std::move(PlaneWeakness_damage_properties),
        tangent_type);
}

}  // namespace PlaneWeakness
}  // namespace Solids
}  // namespace MaterialLib

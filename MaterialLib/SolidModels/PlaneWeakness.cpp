/**
 * \file
 * \author Luca Urpi
 * \date   Sep 7, 2022
 *
 * \copyright
 * Copyright (c) 2012-2022, OpenGeoSys Community (http://www.opengeosys.org)
 *            Distributed under a Modified BSD License.
 *              See accompanying file LICENSE.txt or
 *              http://www.opengeosys.org/project/license
 *
 * OGS's Kelvin vector indices
 *               OGS:    11 22 33 12 23 13
 */

#include "PlaneWeakness.h"

#include <boost/math/special_functions/pow.hpp>
#include <Eigen/LU>
#include <limits>				 

#include "LinearElasticIsotropic.h"
#include "MaterialLib/MPL/Utils/FormEigenTensor.h"
#include "MaterialLib/MPL/Utils/GetSymmetricTensor.h"
#include "MaterialLib/PhysicalConstant.h"
#include "MathLib/LinAlg/Eigen/EigenMapTools.h"

#include <iostream>

namespace MPL = MaterialPropertyLib;

/**
 * Common convenitions for naming:
 * x_D              - deviatoric part of tensor x
 * x_V              - volumetric part of tensor x
 * x_p              - a variable related to plastic potential
 * x_prev           - value of x in previous time step
 *
 * Variables used in the code:
 * eps_D            - deviatoric strain
 * eps_p_D_dot      - deviatoric increment of plastic strain
 * eps_p_eff_dot    - increment of effective plastic strain
 * eps_p_V_dot      - volumetric increment of plastic strain
 * sigma_D_inverse_D - deviatoric part of sigma_D_inverse
 *
 * derivation of the flow rule
 * theta            - J3 / J2^(3 / 2) from yield function
 * dtheta_dsigma    - derivative of theta
 * sqrtPhi          - square root of Phi from plastic potential
 * flow_D           - deviatoric part of flow
 * flow_V           - volumetric part of flow
 * lambda_flow_D    - deviatoric increment of plastic strain
 */

namespace MaterialLib
{
namespace Solids
{
namespace PlaneWeakness
{
template <int DisplacementDim>
double StateVariables<DisplacementDim>::getEquivalentPlasticStrain() const
{
    return std::sqrt(2.0 / 3.0 * Invariants::FrobeniusNorm(eps_p.D.eval()));
}

template <int DisplacementDim>
double StateVariables<DisplacementDim>::getRuptureHappened() const
{
    //std::cout << "output of rupt_flag"<< rupt_flag << "previously " << rupt_flag_prev;
    return rupt_flag;
}

template <int DisplacementDim>
double StateVariables<DisplacementDim>::getShearCumulat() const
{
    return shear_sum;
}


/// Special product of \c v with itself: \f$v \odot v\f$.
/// The tensor \c v is given in Kelvin mapping.
/// \note Implementation only for 2 and 3 dimensions.
/// \attention Pay attention to the sign of the result, which normally would be
/// negative, but the returned value is not negated. This has to do with \f$
/// d(A^{-1})/dA = -A^{-1} \odot A^{-1} \f$.
template <int DisplacementDim>
MathLib::KelvinVector::KelvinMatrixType<DisplacementDim> sOdotS(
    MathLib::KelvinVector::KelvinVectorType<DisplacementDim> const& v);

template <int DisplacementDim>
struct PhysicalStressWithInvariants final
{
    static int const KelvinVectorSize =
        MathLib::KelvinVector::kelvin_vector_dimensions(DisplacementDim);
    using Invariants = MathLib::KelvinVector::Invariants<KelvinVectorSize>;
    using KelvinVector =
        MathLib::KelvinVector::KelvinVectorType<DisplacementDim>;

    explicit PhysicalStressWithInvariants(KelvinVector const& stress)
        : value{stress},
          D{Invariants::deviatoric_projection * stress},
          I_1{Invariants::trace(stress)},
          J_2{Invariants::J2(D)},
          J_3{Invariants::J3(D)}
    {
    }

    KelvinVector value;
    KelvinVector D;
    double I_1;
    double J_2;
    double J_3;

    EIGEN_MAKE_ALIGNED_OPERATOR_NEW;
};

// /// Holds powers of 1 + DilationAngle*theta to base 0, m_p, and m_p-1.
// struct OnePlusDilationAngleTheta final
// {
    // OnePlusDilationAngleTheta(double const DilationAngle, double const theta,
                        // double const m_p)
        // : value{1 + DilationAngle * theta},
          // pow_m_p{std::pow(value, m_p)},
          // pow_m_p1{pow_m_p / value}
    // {
    // }

    // double const value;
    // double const pow_m_p;
    // double const pow_m_p1;
// };

template <int DisplacementDim>
double plasticFlowVolumetricPart(
    PhysicalStressWithInvariants<DisplacementDim> const& s,
    double const sqrtPhi, double const TensionCutOffParameter, double const FrictionAngle,
    double const x_normal, double const y_normal)
{
    return 3 *
               (TensionCutOffParameter * s.I_1 +
                4 * boost::math::pow<2>(x_normal) * boost::math::pow<3>(s.I_1)) /
               (2 * sqrtPhi) +
           3 * FrictionAngle + 6 * y_normal * s.I_1;
}

// template <int DisplacementDim>
// typename PlaneWeakness<DisplacementDim>::KelvinVector plasticFlowDeviatoricPart(
    // PhysicalStressWithInvariants<DisplacementDim> const& s,
    // OnePlusDilationAngleTheta const& one_gt, double const sqrtPhi,
    // typename PlaneWeakness<DisplacementDim>::KelvinVector const& dtheta_dsigma,
    // double const DilationAngle, double const m)
// {
    // return (one_gt.pow_m *
            // (s.D + s.J_2 * m * DilationAngle * dtheta_dsigma / one_gt.value)) /
           // (2 * sqrtPhi);
// }

template <int DisplacementDim>
double yieldFunction(MaterialProperties const& mp,
                     PhysicalStressWithInvariants<DisplacementDim> const& s,
                     double const k)
{
    double const I_1_squared = boost::math::pow<2>(s.I_1);
    assert(s.J_2 != 0);

    return std::sqrt(s.J_2 * std::pow(1 + mp.DilationAngle * s.J_3 /(s.J_2 * std::sqrt(s.J_2)),boost::math::pow<2>(mp.x_normal) *boost::math::pow<2>(I_1_squared)) + mp.FrictionAngle * s.I_1 + mp.y_normal * I_1_squared - k);
                                      //mp.m) +
                     // mp.TensionCutOffParameter / 2. * I_1_squared +
                     // boost::math::pow<2>(mp.x_normal) *
                         // boost::math::pow<2>(I_1_squared)) +
           // mp.FrictionAngle * s.I_1 + mp.y_normal * I_1_squared - k;
}

template <int DisplacementDim>
double StressProjectionPW(MaterialProperties const& mp, 
                       typename PlaneWeakness<DisplacementDim>::KelvinVector const& sigma)
{
	auto const sig  =
         MaterialPropertyLib::formEigenTensor<3>(sigma);
	double Fs = 0.0;
	typename PlaneWeakness<DisplacementDim>::KelvinVector sigma_rot;
	if (DisplacementDim == 2)
	{
		Eigen::Matrix<double, 2, 1> const pw_normal = {mp.x_normal, mp.y_normal};
		Eigen::Matrix<double, 2, 1> const sigma_p = {sig(0,0)*pw_normal(0)+sig(0,1)*pw_normal(1) , sig(1,0)*pw_normal(0)+sig(1,1)*pw_normal(1)} ; // force on pw resulting from stress tensor
		double const sigma_n = sigma_p.dot(pw_normal.transpose()); // normal stress
		double const sigma_s = sigma_p.squaredNorm()- sigma_n*sigma_n; // shear stress
		
		Fs = sigma_s + sigma_n * std::tan(mp.FrictionAngle)- mp.Cohesion; // > 0 then failure (normal stress negative)
		if (sigma_n > 0)	
		{
			Fs = 0.0;
		}
	}
	else
	{
    Eigen::Matrix<double, 3, 3> sigma_matrix;
    sigma_matrix << sigma[0], sigma[3] / std::sqrt(2.), sigma[5] / std::sqrt(2.), sigma[3] / std::sqrt(2.),
         sigma[1], sigma[4] / std::sqrt(2.), sigma[5] / std::sqrt(2.), sigma[4] / std::sqrt(2.),
         sigma[2];
	double nx=mp.x_normal; //angle with x-dir
	double ny=mp.y_normal; //angle with y-dir
	double nz=mp.z_normal; //angle with z-dir
	
	double nzsq=std::sqrt(1-nz*nz);

    Eigen::Matrix<double, 3, 3> A_rot;
    Eigen::Matrix<double, 3, 3> A_rot_t;
    Eigen::Matrix<double, 3, 3> sigma_matrix_rot;
    if (nz==1)
	{
     A_rot << 0,0,0,0,0,0,0,0,1;
	}
	else
	{
     A_rot << -ny/nzsq, -nx/nzsq, 0.0, -nx*nz/nzsq,ny*nz/nzsq, -nzsq,nx,-ny,-nz;
	}
    A_rot_t=A_rot.transpose();
	sigma_matrix_rot=A_rot*sigma_matrix*A_rot_t;
    //sigma_rot=A_rot*sigma*A_rot_t;
    double norm=-sigma_matrix_rot.coeff(2,2);

	if (norm > 0)
    {
		double tau=std::sqrt(sigma_matrix_rot.coeff(2,0)*sigma_matrix_rot.coeff(2,0)+sigma_matrix_rot.coeff(2,1)*sigma_matrix_rot.coeff(2,1));
		Fs = std::abs(tau) - std::abs(norm) * std::tan(mp.FrictionAngle /180.0 *  3.141592653589 )- mp.Cohesion; // > 0 then failure (normal stress negative)
		if (Fs > 0)
		{
			//std::cout << "Normal stress component on the PW is "<< norm <<"\n";
			//std::cout << "Rotated stress is "<< sigma_matrix_rot <<"\n";
			//std::cout << "Orig stress is "<< sigma_matrix <<"\n";
			//std::cout << "Fs is "<< Fs <<"\n";
		}
	}
	else
	{
		//std::cout << "Tensile condiitons"<<"\n";
		//Fs= 0.0;
	}
		
    }
	
    return Fs;
}
 
/* template <int DisplacementDim>
 typename PlaneWeakness<DisplacementDim>::KelvinVector StressCorrectionPW(MaterialProperties const& mp, 
                       typename PlaneWeakness<DisplacementDim>::KelvinVector const& sigma)
{
	auto const sig  =
         MaterialPropertyLib::formEigenTensor<3>(sigma);
	typename PlaneWeakness<DisplacementDim>::KelvinVector sigma_new = 
			sigma ;
	Eigen::Matrix<double, 3, 3> sigma_matrix_rot;
	Eigen::Matrix<double, 3, 3> sigma_correct_back_rotated;
	Eigen::Matrix<double, 3, 3> sigma_matrix_rot_correct;
	typename PlaneWeakness<DisplacementDim>::KelvinVector sigma_out;
    if (DisplacementDim == 2)
	{
		std::cout << "Error, PW not defined in 2-dimensions "; 
	}
	else
	{
	    // std::cout << "sigma components:\n Sigma0 " << sigma[0]
              // << "\n Sigma1 " << sigma[1]
              // << "\n Sigma2 " << sigma[2]
              // << "\n Sigma3 " << sigma[3]
              // << "\n Sigma4 " << sigma[4]
              // << "\n Sigma5 " << sigma[5] <<"\n";

    // std::cout << "transform sigmaKelvin into sigma_matrix\n";
    Eigen::Matrix<double, 3, 3> sigma_matrix;
     sigma_matrix << sigma[0], sigma[3] / std::sqrt(2.), sigma[5] / std::sqrt(2.), sigma[3] / std::sqrt(2.),
         sigma[1], sigma[4] / std::sqrt(2.), sigma[5] / std::sqrt(2.), sigma[4] / std::sqrt(2.),
         sigma[2];
	

    //std::cout << "the matrix is\n" << sigma_matrix<<"\n";

    //std::cout << "Rotate sigma_matrix\n";
    // given the normal nx ny nz, the rotation is obtained as  
	// sigma_matrix_rot = A sigma_matrix A_transposed 
	// where A is obtained from the normal nx ny nz of the weakness plane
    double nx=mp.x_normal; //std::cos(90); //angle with x-dir
	double ny=mp.y_normal; //std::cos(45); //angle with y-dir
	double nz=mp.z_normal; //std::sqrt(1-ny*ny); //std::cos(45); //angle with z-dir
	//std::cout << "the normal vector is\n" << nx << " " << ny  <<" "<< nz<<"\n";

    double nzsq=std::sqrt(1-nz*nz);

    Eigen::Matrix<double, 3, 3> A_rot;
    Eigen::Matrix<double, 3, 3> A_rot_t;
    Eigen::Matrix<double, 3, 3> sigma_matrix_rot;
    if (nz==1)
	{
     A_rot << 0,0,0,0,0,0,0,0,1;
	}
	else
	{
     A_rot << -ny/nzsq, -nx/nzsq, 0.0, -nx*nz/nzsq,ny*nz/nzsq, -nzsq,nx,-ny,-nz;
	}
    A_rot_t=A_rot.transpose();
	sigma_matrix_rot=A_rot*sigma_matrix*A_rot_t;
    //std::cout << "the rotated matrix is\n" << sigma_matrix_rot <<"\n";
    // std::cout << "coeff(3,3)" <<sigma_matrix_rot.coeff(3,3)<<"\n";
    // std::cout << "coeff(2,2)" <<sigma_matrix_rot.coeff(2,2)<<"\n";
    // std::cout << "coeff(1,1)" <<sigma_matrix_rot.coeff(1,1)<<"\n";
    // std::cout << "coeff(2,1)" <<sigma_matrix_rot.coeff(2,1)<<"\n";
	

    
    double norm=-sigma_matrix_rot.coeff(2,2);
    double tau=std::sqrt(sigma_matrix_rot.coeff(2,0)*sigma_matrix_rot.coeff(2,0)+sigma_matrix_rot.coeff(2,1)*sigma_matrix_rot.coeff(2,1));
	double delta_tau = tau - norm * std::tan(mp.FrictionAngle /180.0 *  3.141592653589 )- mp.Cohesion; // > 0 then failure (normal stress negative)
	// std::cout << "\n Yield Function Fs=" << Fs <<" = " << tau <<" - cohesion - tan(frict)*" << norm << "\n";
	sigma_matrix_rot_correct << sigma_matrix_rot.coeff(0,0), sigma_matrix_rot.coeff(0,1), sigma_matrix_rot.coeff(0,2)*(1+delta_tau/tau),  sigma_matrix_rot.coeff(1,0), sigma_matrix_rot.coeff(1,1),sigma_matrix_rot.coeff(1,2)*(1+delta_tau/tau),sigma_matrix_rot.coeff(2,0)*(1+delta_tau/tau), sigma_matrix_rot.coeff(2,1)*(1+delta_tau/tau),sigma_matrix_rot.coeff(2,2);
	//sigma_matrix_rot(2,1)*=(1+delta_tau/tau);
	sigma_correct_back_rotated=A_rot_t*sigma_matrix_rot_correct*A_rot;
    sigma_new << sigma_matrix_rot.coeff(0,0), sigma_matrix_rot.coeff(1,1),sigma_matrix_rot.coeff(2,2),std::sqrt(2.)*sigma_matrix_rot.coeff(0,1), std::sqrt(2.)*sigma_matrix_rot.coeff(1,2), std::sqrt(2.)*sigma_matrix_rot.coeff(0,2);
	std::cout << "the back-rotated matrix is\n" << sigma_correct_back_rotated <<"\n";
    
    // std::cout << "Friction Angle: " << std::tan(mp.FrictionAngle/180.0 *  3.141592653589) <<"  cohesion: " << mp.cohesion << "\n";
	
	
	 
    //sigma[0]=sigma_correct_back_rotated.coeff(0,0);
    //sigma[1]=sigma_correct_back_rotated.coeff(1,1);
    //sigma[2]=sigma_correct_back_rotated.coeff(2,2);
    //sigma[3]=std::sqrt(2.)*sigma_correct_back_rotated.coeff(0,1);
    //sigma[4]=std::sqrt(2.)*sigma_correct_back_rotated.coeff(1,2);
    //sigma[5]=std::sqrt(2.)*sigma_correct_back_rotated.coeff(0,2);
	}
	return sigma_new; 
    //return sigma_matrix_rot;
}   
template <int DisplacementDim>
double yieldFunctionPW(MaterialProperties const& mp, 
                       typename PlaneWeakness<DisplacementDim>::KelvinVector const& s)
{
	// to be done: shear stress vs normal stress
    return mp.FrictionAngle;
}
*/

/* template <int DisplacementDim>
typename PlaneWeakness<DisplacementDim>::KelvinVector predict_sigma(
    double const G, double const K,
    typename PlaneWeakness<DisplacementDim>::KelvinVector const& sigma_prev,
    typename PlaneWeakness<DisplacementDim>::KelvinVector const& eps_m,
    typename PlaneWeakness<DisplacementDim>::KelvinVector const& eps_m_prev,
    double const eps_V)
{
    static int const KelvinVectorSize =
        MathLib::KelvinVector::kelvin_vector_dimensions(DisplacementDim);
		
    using Invariants = MathLib::KelvinVector::Invariants<KelvinVectorSize>;
    auto const& P_dev = Invariants::deviatoric_projection;
	
    const auto C = elasticTangentStiffness<DisplacementDim>(K - 2. / 3 * G, G);;
    typename PlaneWeakness<DisplacementDim>::KelvinVector sigma_try = 
			sigma_prev + C * (eps_m - eps_m_prev);	



    // // dimensionless initial hydrostatic pressure
    // double const pressure_prev = Invariants::trace(sigma_prev) / (-3. * G);
    // // initial strain invariant
    // double const e_prev = Invariants::trace(eps_prev);
    // // dimensioness hydrostatic stress increment
    // double const pressure = pressure_prev - K / G * (eps_V - e_prev);
    // // dimensionless deviatoric initial stress
    // typename PlaneWeakness<DisplacementDim>::KelvinVector const sigma_D_prev =
        // P_dev * sigma_prev / G;
    // // dimensionless deviatoric stress
    // typename PlaneWeakness<DisplacementDim>::KelvinVector const sigma_D =
        // sigma_D_prev + 2 * P_dev * (y_normal - eps_prev);
    // return sigma_D - pressure * Invariants::identity2;
	return sigma_try;
} */
template <int DisplacementDim>
typename PlaneWeakness<DisplacementDim>::KelvinVector kvector_rot_to_local(
	MaterialProperties const& mp, 
	typename PlaneWeakness<DisplacementDim>::KelvinVector const& k_vec)
{
    //static int const KelvinVectorSize =
      //  MathLib::KelvinVector::kelvin_vector_dimensions(DisplacementDim);
		
    
    double nx=mp.x_normal; //std::cos(90); //angle with x-dir
	double ny=mp.y_normal; //std::cos(45); //angle with y-dir
	double nz=mp.z_normal; //std::sqrt(1-ny*ny); //std::cos(45); //angle with z-dir
	//std::cout << "the normal vector is\n" << nx << " " << ny  <<" "<< nz<<"\n";

    double nzsq=std::sqrt(1-nz*nz);

    Eigen::Matrix<double, 3, 3> A_rot;
    Eigen::Matrix<double, 3, 3> A_rot_t;
    Eigen::Matrix<double, 3, 3> k_matrix;
    Eigen::Matrix<double, 3, 3> k_matrix_rot;
	typename PlaneWeakness<DisplacementDim>::KelvinVector k_vec_rot;
    k_matrix << k_vec[0], k_vec[3], k_vec[5], 
				     k_vec[3], k_vec[1], k_vec[4], 
				     k_vec[5], k_vec[4], k_vec[2];
    if (nz==1)
	{
     A_rot << 0,0,0,0,0,0,0,0,1;
	}
	else
	{
     A_rot << -ny/nzsq, -nx/nzsq, 0.0, -nx*nz/nzsq,ny*nz/nzsq, -nzsq,nx,-ny,-nz;
	}
    A_rot_t=A_rot.transpose();
	k_matrix_rot=A_rot*k_matrix*A_rot_t;
    //std::cout << "the rotated matrix is\n" << sigma_matrix_rot <<"\n";
    k_vec_rot << k_matrix_rot.coeff(0,0), k_matrix_rot.coeff(1,1), k_matrix_rot.coeff(2,2),  k_matrix_rot.coeff(0,1), k_matrix_rot.coeff(1,2),k_matrix_rot.coeff(0,2);
	//sigma_matrix_rot(2,1)*=(1+delta_tau/tau);
	
    return k_vec_rot;
}
template <int DisplacementDim>
typename PlaneWeakness<DisplacementDim>::KelvinVector kvector_rot_to_global(
	MaterialProperties const& mp, 
	typename PlaneWeakness<DisplacementDim>::KelvinVector const& k_vec)
{
    //static int const KelvinVectorSize =
      //  MathLib::KelvinVector::kelvin_vector_dimensions(DisplacementDim);
		
    
    double nx=mp.x_normal; //std::cos(90); //angle with x-dir
	double ny=mp.y_normal; //std::cos(45); //angle with y-dir
	double nz=mp.z_normal; //std::sqrt(1-ny*ny); //std::cos(45); //angle with z-dir
	//std::cout << "the normal vector is\n" << nx << " " << ny  <<" "<< nz<<"\n";

    double nzsq=std::sqrt(1-nz*nz);

    Eigen::Matrix<double, 3, 3> A_rot;
    Eigen::Matrix<double, 3, 3> A_rot_t;
    Eigen::Matrix<double, 3, 3> k_matrix;
    Eigen::Matrix<double, 3, 3> k_matrix_rot;
	typename PlaneWeakness<DisplacementDim>::KelvinVector k_vec_rot;
    k_matrix << k_vec[0], k_vec[3], k_vec[5], 
				     k_vec[3], k_vec[1], k_vec[4], 
				     k_vec[5], k_vec[4], k_vec[2];
    if (nz==1)
	{
     A_rot << 0,0,0,0,0,0,0,0,1;
	}
	else
	{
     A_rot << -ny/nzsq, -nx/nzsq, 0.0, -nx*nz/nzsq,ny*nz/nzsq, -nzsq,nx,-ny,-nz;
	}
    A_rot_t=A_rot.transpose();
	k_matrix_rot=A_rot_t*k_matrix*A_rot;
    //std::cout << "the rotated matrix is\n" << sigma_matrix_rot <<"\n";
    k_vec_rot << k_matrix_rot.coeff(0,0), k_matrix_rot.coeff(1,1), k_matrix_rot.coeff(2,2),  k_matrix_rot.coeff(0,1), k_matrix_rot.coeff(1,2),k_matrix_rot.coeff(0,2);
	//sigma_matrix_rot(2,1)*=(1+delta_tau/tau);
	
    return k_vec_rot;
}

template <int DisplacementDim>
typename PlaneWeakness<DisplacementDim>::KelvinVector sigma_rotation(
    double const nx, double const ny, double const nz)
{
    static int const KelvinVectorSize =
        MathLib::KelvinVector::kelvin_vector_dimensions(DisplacementDim);
		
    
    // std::cout << "Compute rotation matrix \n";
    // given the normal nx ny nz, the rotation is obtained as  
	// sigma_matrix_rot = A sigma_matrix A_transposed 
	// where A is obtained from the normal nx ny nz of the weakness plane
    // std::cout << "the normal vector is\n" << nx << " " << ny  <<" "<< nz<<"\n";

    Eigen::Matrix<double, 3, 3> A_rot;
    if (nz==1)
	{
     A_rot << 0,0,0,0,0,0,0,0,1;
	}
	else
	{
	 double nzsq=std::sqrt(1-nz*nz);
     A_rot << -ny/nzsq, -nx/nzsq, 0.0, -nx*nz/nzsq,ny*nz/nzsq, -nzsq,nx,-ny,-nz;
	}
    


    return A_rot;
}
template <int DisplacementDim>
typename PlaneWeakness<DisplacementDim>::KelvinVector sigma_try_corrected(
    double const nx, double const ny, double const nz,
    typename PlaneWeakness<DisplacementDim>::KelvinVector const& sigma_try,
	double const tau)
    
    
{
    static int const KelvinVectorSize =
        MathLib::KelvinVector::kelvin_vector_dimensions(DisplacementDim);
		
    
    
    Eigen::Matrix<double, 3, 3> A_rot;
    Eigen::Matrix<double, 3, 3> A_rot_t;
    if (nz==1)
	{
     A_rot << 0,0,0,0,0,0,0,0,1;
	}
	else
	{
     double nzsq=std::sqrt(1-nz*nz);
     A_rot << -ny/nzsq, -nx/nzsq, 0.0, -nx*nz/nzsq,ny*nz/nzsq, -nzsq,nx,-ny,-nz;
	}
     
	
	A_rot_t=A_rot.transpose();
	// build matrix for computations
	Eigen::Matrix<double, 3, 3> sigma_matrix;
	Eigen::Matrix<double, 3, 3> sigma_matrix_rot;
	Eigen::Matrix<double, 3, 3> sigma_new;
	Eigen::Matrix<double, 3, 3> sigma_correct_back_rotated;
     sigma_matrix << sigma_try[0], sigma_try[3], sigma_try[5], 
				     sigma_try[3], sigma_try[1], sigma_try[4], 
				     sigma_try[5], sigma_try[4], sigma_try[2];
		 
    sigma_matrix_rot=A_rot*sigma_matrix*A_rot_t;
	// OGS:    11 22 33 12 23 13
    // OGS_plane of weakness:    1'1' 2'2' norm 1'2' 2'3' 1'3'
    sigma_new << sigma_matrix_rot.coeff(0,0), sigma_matrix_rot.coeff(1,1),sigma_matrix_rot.coeff(2,2),std::sqrt(2.)*sigma_matrix_rot.coeff(0,1), std::sqrt(2.)*sigma_matrix_rot.coeff(1,2), std::sqrt(2.)*sigma_matrix_rot.coeff(0,2);

	


    return sigma_new;
}
// /// Split the agglomerated solution vector in separate items. The arrangement
// /// must be the same as in the newton() function.
// template <typename ResidualVector, typename KelvinVector>
// std::tuple<KelvinVector, PlasticStrain<KelvinVector>, double>
// splitSolutionVector(ResidualVector const& solution)
// {
    // static auto const size = KelvinVector::SizeAtCompileTime;
    // return std::forward_as_tuple(
        // solution.template segment<size>(size * 0),
        // PlasticStrain<KelvinVector>{solution.template segment<size>(size * 1),
                                    // solution[size * 2], solution[size * 2 + 1]},
        // solution[size * 2 + 2]);
// }

template <int DisplacementDim>
PlaneWeakness<DisplacementDim>::PlaneWeakness(
    NumLib::NewtonRaphsonSolverParameters nonlinear_solver_parameters,
    MaterialPropertiesParameters material_properties,
    std::unique_ptr<DamagePropertiesParameters>&& damage_properties,
    TangentType tangent_type)
    : _nonlinear_solver_parameters(std::move(nonlinear_solver_parameters)),
      _mp(std::move(material_properties)),
      _damage_properties(std::move(damage_properties)),
      _tangent_type(tangent_type)
{
}

template <int DisplacementDim>
double PlaneWeakness<DisplacementDim>::computeFreeEnergyDensity(
    double const /*t*/,
    ParameterLib::SpatialPosition const& /*x*/,
    double const /*dt*/,
    KelvinVector const& y_normal,
    KelvinVector const& sigma,
    typename MechanicsBase<DisplacementDim>::MaterialStateVariables const&
        material_state_variables) const
{
    assert(dynamic_cast<StateVariables<DisplacementDim> const*>(
               &material_state_variables) != nullptr);

    auto const& eps_p = static_cast<StateVariables<DisplacementDim> const&>(
                            material_state_variables)
                            .eps_p;
    using Invariants = MathLib::KelvinVector::Invariants<KelvinVectorSize>;
    auto const& identity2 = Invariants::identity2;
    return (y_normal - eps_p.D - eps_p.V / 3 * identity2).dot(sigma) / 2;
}

template <int DisplacementDim>
std::optional<std::tuple<typename PlaneWeakness<DisplacementDim>::KelvinVector,
                         std::unique_ptr<typename MechanicsBase<
                             DisplacementDim>::MaterialStateVariables>,
                         typename PlaneWeakness<DisplacementDim>::KelvinMatrix>>
PlaneWeakness<DisplacementDim>::integrateStress(
    MaterialPropertyLib::VariableArray const& variable_array_prev,
    MaterialPropertyLib::VariableArray const& variable_array, double const t,
    ParameterLib::SpatialPosition const& x, double const dt,
    typename MechanicsBase<DisplacementDim>::MaterialStateVariables const&
    material_state_variables) const
{
    auto const& eps_m = std::get<MPL::SymmetricTensor<DisplacementDim>>(
        variable_array.mechanical_strain);
    auto const& eps_m_prev = std::get<MPL::SymmetricTensor<DisplacementDim>>(
        variable_array_prev.mechanical_strain);
    auto const& sigma_prev = std::get<MPL::SymmetricTensor<DisplacementDim>>(
        variable_array_prev.stress);
    // auto const& plastic_eps = std::get<MPL::SymmetricTensor<DisplacementDim>>(
        // variable_array_prev.equivalent_plastic_strain);

    auto const& rupt_flag_prev = static_cast<StateVariables<DisplacementDim> const&>(
                            material_state_variables)
                            .rupt_flag;
    auto const& shear_sum_prev = static_cast<StateVariables<DisplacementDim> const&>(
                            material_state_variables)
                            .shear_sum;
    //auto& state =
      //               static_cast<StateVariables<DisplacementDim>&>(material_state_variables);
    assert(dynamic_cast<StateVariables<DisplacementDim> const*>(
               &material_state_variables) != nullptr);

    StateVariables<DisplacementDim> state =
        static_cast<StateVariables<DisplacementDim> const&>(
            material_state_variables);
    state.setInitialConditions();
    //std::cout << "initialization in InternalStress "<< state.shear_sum << " previously " << state.shear_sum_prev << "\n";
    
    using Invariants = MathLib::KelvinVector::Invariants<KelvinVectorSize>;
    
	Eigen::FullPivLU<Eigen::Matrix<double, KelvinVectorSize, KelvinVectorSize,
                                   Eigen::RowMajor>>
        linear_solver;
    // // volumetric strain
    // double const eps_V = Invariants::trace(eps_m);
    //std::cout << "Initialized FullPivLU values for PW model local linear solver \n";
    //auto const& P_dev = Invariants::deviatoric_projection;
    // // deviatoric strain
    // KelvinVector const eps_D = P_dev * eps_m;

    MaterialProperties const mp(t, x, _mp);
    const auto C = elasticTangentStiffness<DisplacementDim>(mp.K - 2. / 3 * mp.G, mp.G);
    
    state.rupt_flag = rupt_flag_prev;
	// if (state.rupt_flag_prev > 0 )
	// {
    // std::cout << "internal calcualtion "<< state.rupt_flag << " previously " << state.rupt_flag_prev << "\n";
    // }
    KelvinVector sigma_try = sigma_prev + C * (eps_m - eps_m_prev); //KelvinVector sigma = predict_sigma<DisplacementDim>(
        //mp.G, mp.K, sigma_prev, eps_m, eps_m_prev, eps_V);

    KelvinMatrix tangentStiffness;
    
    double const Fs=StressProjectionPW<DisplacementDim>(mp, sigma_try); //evaluate if stress state plastic
	// ALGUR add lines 120-130 from Coulomb.cpp??

    //std::cout << "Calculated if yield > 0, Fs =  " << Fs << " \n";
    // Quit early if sigma is zero (nothing to do) or if we are still in elastic
    // zone.
	if (sigma_try.squaredNorm() == 0 ) 
    {
        //tangentStiffness = elasticTangentStiffness<DisplacementDim>(mp.K - 2. / 3 * mp.G, mp.G);
		state.rupt_flag = state.rupt_flag_prev;
        return {std::make_tuple(
            sigma_try,
            std::unique_ptr<typename MechanicsBase<
                DisplacementDim>::MaterialStateVariables>{
                new StateVariables<DisplacementDim>{
                    static_cast<StateVariables<DisplacementDim> const&>(
                        state)}},
            C)};
    }
	
    if (Fs < 0)
    {
        //std::cout << "No Yield, returning the stress guess " << sigma_try << " \n";
        state.rupt_flag = state.rupt_flag_prev;
        return {std::make_tuple(
            sigma_try,
            std::unique_ptr<typename MechanicsBase<
                DisplacementDim>::MaterialStateVariables>{
                new StateVariables<DisplacementDim>{
                    static_cast<StateVariables<DisplacementDim> const&>(
                        state)}},
            C)};
    }
	else
	{
	double plast_mult;
    const auto G_mod = mp.G ; // will be Fs/ (G+ (K+4/3G)*tan(dilation)*tan(friction))
    //std::cout << " PRE LOCAL ITERATION: sigma_try is  " << sigma_try <<", PRE LOCAL ITERATION: sigma prev is "<< sigma_prev<< " \n";    
    // OGS:    11 22 33 12 23 13
    // OGS_plane of weakness:    1'1' 2'2' norm 1'2' 2'3' 1'3'	
    KelvinVector sigma_shear_mask;
    KelvinVector sigma_correction;
    KelvinVector solution_loc;
    KelvinVector sigma_prev_loc;
    KelvinVector sigma_try_loc;
    KelvinVector sigma_correction_globa;
    //sigma_shear_mask << 0,0,0,0,sigma_try[4]/sqrt(sigma_try[4]*sigma_try[4] + sigma_try[5]* sigma_try[5]),sigma_try[5]/sqrt(sigma_try[4]*sigma_try[4] + sigma_try[5]* sigma_try[5]);
    sigma_shear_mask << 0,0,0,0,1,0;
    
	ResidualVectorType solution = sigma_try;// only barebone setting, if yield, update eps_p (non-zero value) and output elastic stress computation
    sigma_correction=Fs * sigma_shear_mask;
	sigma_correction_globa=kvector_rot_to_global<DisplacementDim>(mp, sigma_correction);
    solution_loc=kvector_rot_to_local<DisplacementDim>(mp, solution);
    sigma_prev_loc=kvector_rot_to_local<DisplacementDim>(mp, sigma_prev);
	sigma_try_loc=kvector_rot_to_local<DisplacementDim>(mp, sigma_try);
	
	double const phi = std::tan(mp.FrictionAngle/180.0 *  3.141592653589);
	plast_mult=Fs/mp.G;
	if (sigma_try_loc[4] < 0)
		{plast_mult=-Fs/mp.G;} 
	
    auto const update_jacobian = [&solution_loc, &Fs, &plast_mult](JacobianMatrix& jacobian)
    {
        auto const& D = Invariants::deviatoric_projection;
        KelvinVector sigma_shear_mask;
		sigma_shear_mask << 0,0,0,0,1,0;//sigma_shear_mask << 0,0,0,0,solution_loc[4]/sqrt(solution_loc[4]*solution_loc[4] + solution_loc[5]* solution_loc[5]),solution_loc[5]/sqrt(solution_loc[4]*solution_loc[4] + solution_loc[5]* solution_loc[5]);
		KelvinVector const s_n1 = (sigma_shear_mask*sigma_shear_mask.transpose())*solution_loc;
        //KelvinVector const s_n1 = D * sigma_shear_mask;
        double const norm_s_n1 = Invariants::FrobeniusNorm(s_n1); //
        jacobian = KelvinMatrix::Identity()+ plast_mult*std::pow(norm_s_n1, - 2) * s_n1 * s_n1.transpose() ;//+/C
        //std::cout << "Jacobian is  " << jacobian <<" \n";
       // std::cout << "Jacobian directions given by D "  << D <<" \n";
       // std::cout << "s_n1  " << s_n1 <<" \n";
	   //std::cout << "s_n1  " << s_n1 << " \n s_n1  transp" << s_n1.transpose() << " \n their product  " << s_n1 * s_n1.transpose() <<" \n";
	
    };    
	//std::cout << "locate at position Prev solution is  " << sigma_prev_loc <<" \n";
	// std::cout << "Fs from prev was " << StressProjectionPW<DisplacementDim>(mp, kvector_rot_to_global<DisplacementDim>(mp, sigma_prev_loc))<<" \n";
	//std::cout << "current try solution is  " << sigma_try_loc <<" \n";
	// std::cout << "Fs from trial is " << StressProjectionPW<DisplacementDim>(mp, kvector_rot_to_global<DisplacementDim>(mp, sigma_try_loc))<<" \n";

    auto const update_residual =
        [&solution_loc, &G_mod, &plast_mult,  &sigma_try_loc](ResidualVectorType& r)
    {
        auto const& D = Invariants::deviatoric_projection;
        KelvinVector sigma_shear_mask ;
		sigma_shear_mask << 0,0,0,0,1,0;//sigma_shear_mask << 0,0,0,0,solution_loc[4]/sqrt(solution_loc[4]*solution_loc[4] + solution_loc[5]* solution_loc[5]),solution_loc[5]/sqrt(solution_loc[4]*solution_loc[4] + solution_loc[5]* solution_loc[5]);
		KelvinVector const s_n1 = (sigma_shear_mask*sigma_shear_mask.transpose())*solution_loc;
        //KelvinVector const s_n1 = D * sigma_shear_mask;
        //KelvinVector const s_n1 = Invariants::deviatoric_projection * solution_loc;
        double const norm_s_n1 = Invariants::FrobeniusNorm(s_n1);
        r = solution_loc - sigma_try_loc + plast_mult * G_mod * sigma_shear_mask; //std::pow(norm_s_n1, -1)* s_n1;//+ pow_norm_s_n1_n_minus_one_2b_G * s_n1;
		//std::cout << "Sigma_try is  " << sigma_try_loc <<" \n";
		// std::cout << "+correction is  " << plast_mult * G_mod * sigma_shear_mask; //2*G_mod * std::pow(norm_s_n1, -1)* s_n1 <<" \n";
        //std::cout << "Residual is  " << r <<" \n";
        //std::cout << "Deviatoric Projection  is  " << D <<" \n";
        //std::cout << "Sigma_mask is  " << sigma_shear_mask <<" \n";
        //std::cout << "Sigma_mask projection is  " << sigma_shear_mask*sigma_shear_mask.transpose() <<" \n";
        // std::cout << "residual is " << r << " , while current local solution is  " << solution_loc <<" \n";
	    // std::cout << "current trial solution is  " << sigma_try_loc <<" \n";

	};
    auto const update_solution = [&](ResidualVectorType const& increment)
    { solution_loc += increment;
      //std::cout << "New local solution is  " << solution_loc <<" \n";
      //std::cout << "Sigma_try_loc is  " << sigma_try_loc <<" \n";
      //std::cout << "Increment is  " << increment <<" \n";
	  sigma_correction_globa=kvector_rot_to_global<DisplacementDim>(mp, solution_loc);
      //std::cout << "New global solution is  " << sigma_correction_globa <<" \n";
	
    };

    auto newton_solver =
        NumLib::NewtonRaphson<decltype(linear_solver), JacobianMatrix,
                              decltype(update_jacobian), ResidualVectorType,
                              decltype(update_residual),
                              decltype(update_solution)>(
            linear_solver, update_jacobian, update_residual, update_solution,
            _nonlinear_solver_parameters);

    JacobianMatrix jacobian;
    auto const success_iterations = newton_solver.solve(jacobian);

    // std::cout << "Fs after NewtonRaphson is " << StressProjectionPW<DisplacementDim>(mp, kvector_rot_to_global<DisplacementDim>(mp, solution_loc))<<" \n";


    if (!success_iterations)
    {
        std::cout << " Local Iteration failed. \n";
        return {};
    }

    // // If *success_iterations>0, tangentStiffness = J_(sigma)^{-1}C
    // // where J_(sigma) is the Jacobian of the last local Newton-Raphson
    // // iteration, which is already LU decomposed.
    // tangentStiffness =
        // (*success_iterations == 0) ? C : linear_solver.solve(C);	
	sigma_correction_globa=kvector_rot_to_global<DisplacementDim>(mp, solution_loc);
	//std::cout << "Try local solution was  " << sigma_try_loc <<" \n"; 
	//std::cout << "New local solution is  " << solution_loc <<" \n";
	//std::cout << "New solution is  " << sigma_correction_globa <<" \n";
      //plastic_eps = eps_m; //-eps_m_prev //Invariants::FrobeniusNorm(eps_m-eps_m_prev); // + plastic_eps 
    //state.eps_p =
      //       plastic_eps;
    //need to be checked if rupt_flag already assigned (i.e. first rupture already happened)

    tangentStiffness = (*success_iterations == 0) ? C : linear_solver.solve(C);
    if (state.rupt_flag_prev>0.0)
	{
		state.rupt_flag = state.rupt_flag_prev;
		state.shear_sum = state.shear_sum_prev+std::abs(sigma_prev_loc[4]-sigma_try_loc[4]);
	}
	else
	{
		state.rupt_flag = std::abs(sigma_try_loc[2])+1.0;//solution_loc[2];
	    state.rupt_flag_prev = std::abs(sigma_try_loc[2])+1.0;//solution_loc[2];
		state.shear_sum = std::abs(sigma_prev_loc[4]-sigma_try_loc[4]);
		state.shear_sum_prev = Fs;
		//state.shear_sum=5e-6;
	}	
	
    KelvinVector sigma_try_loc_corrected;
    KelvinVector sigma_try_corrected;
	
	sigma_try_loc_corrected << sigma_try_loc[0], sigma_try_loc[1], sigma_try_loc[2],  sigma_try_loc[3], sigma_try_loc[4],sigma_try_loc[5];
	
	sigma_try_corrected=kvector_rot_to_global<DisplacementDim>(mp, sigma_try_loc_corrected);
    // return {std::make_tuple(
        // sigma_correction_globa,
        // std::unique_ptr<
            // typename MechanicsBase<DisplacementDim>::MaterialStateVariables>{
            // new StateVariables<DisplacementDim>{
                // static_cast<StateVariables<DisplacementDim> const&>(state)}},
        // tangentStiffness)};
        // BEGIN do return elastic even when failure
		// return {std::make_tuple(
            // sigma_try_corrected,
            // std::unique_ptr<typename MechanicsBase<
                // DisplacementDim>::MaterialStateVariables>{
                // new StateVariables<DisplacementDim>{
                    // static_cast<StateVariables<DisplacementDim> const&>(
                        // state)}},
            // C)};
			
        // END do return elastic even when failure
        return {std::make_tuple(
            sigma_correction_globa,
            std::unique_ptr<typename MechanicsBase<
                DisplacementDim>::MaterialStateVariables>{
                new StateVariables<DisplacementDim>{
                    static_cast<StateVariables<DisplacementDim> const&>(
                        state)}},
            C)};	
	}
    //return {std::make_tuple(sigma_try,std::make_unique<typename MechanicsBase<DisplacementDim>::MaterialStateVariables>(), C)};
}

template <int DisplacementDim>
std::vector<typename MechanicsBase<DisplacementDim>::InternalVariable>
PlaneWeakness<DisplacementDim>::getInternalVariables() const
{
    return {{"damage.cohesion_d", 1,
             [](typename MechanicsBase<
                    DisplacementDim>::MaterialStateVariables const& state,
                std::vector<double>& cache) -> std::vector<double> const& {
                 assert(dynamic_cast<StateVariables<DisplacementDim> const*>(
                            &state) != nullptr);
                 auto const& PlaneWeakness_state =
                     static_cast<StateVariables<DisplacementDim> const&>(state);

                 cache.resize(1);
                 cache.front() = PlaneWeakness_state.damage.cohesion_d();
                 return cache;
             },
             [](typename MechanicsBase<DisplacementDim>::MaterialStateVariables&
                    state) -> BaseLib::DynamicSpan<double> {
                 assert(dynamic_cast<StateVariables<DisplacementDim> const*>(
                            &state) != nullptr);
                 auto& PlaneWeakness_state =
                     static_cast<StateVariables<DisplacementDim>&>(state);

                 return {&PlaneWeakness_state.damage.cohesion_d(), 1};
             }},
            {"damage.value", 1,
             [](typename MechanicsBase<
                    DisplacementDim>::MaterialStateVariables const& state,
                std::vector<double>& cache) -> std::vector<double> const& {
                 assert(dynamic_cast<StateVariables<DisplacementDim> const*>(
                            &state) != nullptr);
                 auto const& PlaneWeakness_state =
                     static_cast<StateVariables<DisplacementDim> const&>(state);

                 cache.resize(1);
                 cache.front() = PlaneWeakness_state.damage.value();
                 return cache;
             },
             [](typename MechanicsBase<DisplacementDim>::MaterialStateVariables&
                    state) -> BaseLib::DynamicSpan<double> {
                 assert(dynamic_cast<StateVariables<DisplacementDim> const*>(
                            &state) != nullptr);
                 auto& PlaneWeakness_state =
                     static_cast<StateVariables<DisplacementDim>&>(state);

                 return {&PlaneWeakness_state.damage.value(), 1};
             }},
            {"eps_p.D", KelvinVector::RowsAtCompileTime,
             [](typename MechanicsBase<
                    DisplacementDim>::MaterialStateVariables const& state,
                std::vector<double>& cache) -> std::vector<double> const& {
                 assert(dynamic_cast<StateVariables<DisplacementDim> const*>(
                            &state) != nullptr);
                 auto const& PlaneWeakness_state =
                     static_cast<StateVariables<DisplacementDim> const&>(state);

                 cache.resize(KelvinVector::RowsAtCompileTime);
                 MathLib::toVector<KelvinVector>(
                     cache, KelvinVector::RowsAtCompileTime) =
                     MathLib::KelvinVector::kelvinVectorToSymmetricTensor(
                         PlaneWeakness_state.eps_p.D);

                 return cache;
             },
             [](typename MechanicsBase<DisplacementDim>::MaterialStateVariables&
                    state) -> BaseLib::DynamicSpan<double> {
                 assert(dynamic_cast<StateVariables<DisplacementDim> const*>(
                            &state) != nullptr);
                 auto& PlaneWeakness_state =
                     static_cast<StateVariables<DisplacementDim>&>(state);

                 return {
                     PlaneWeakness_state.eps_p.D.data(),
                     static_cast<std::size_t>(KelvinVector::RowsAtCompileTime)};
             }},
            {"eps_p.V", 1,
             [](typename MechanicsBase<
                    DisplacementDim>::MaterialStateVariables const& state,
                std::vector<double>& cache) -> std::vector<double> const& {
                 assert(dynamic_cast<StateVariables<DisplacementDim> const*>(
                            &state) != nullptr);
                 auto const& PlaneWeakness_state =
                     static_cast<StateVariables<DisplacementDim> const&>(state);

                 cache.resize(1);
                 cache.front() = PlaneWeakness_state.eps_p.V;
                 return cache;
             },
             [](typename MechanicsBase<DisplacementDim>::MaterialStateVariables&
                    state) -> BaseLib::DynamicSpan<double> {
                 assert(dynamic_cast<StateVariables<DisplacementDim> const*>(
                            &state) != nullptr);
                 auto& PlaneWeakness_state =
                     static_cast<StateVariables<DisplacementDim>&>(state);

                 return {&PlaneWeakness_state.eps_p.V, 1};
             }},
            {"eps_p.eff", 1,
             [](typename MechanicsBase<
                    DisplacementDim>::MaterialStateVariables const& state,
                std::vector<double>& cache) -> std::vector<double> const& {
                 assert(dynamic_cast<StateVariables<DisplacementDim> const*>(
                            &state) != nullptr);
                 auto const& PlaneWeakness_state =
                     static_cast<StateVariables<DisplacementDim> const&>(state);

                 cache.resize(1);
                 cache.front() = PlaneWeakness_state.eps_p.eff;
                 return cache;
             },
             [](typename MechanicsBase<DisplacementDim>::MaterialStateVariables&
                    state) -> BaseLib::DynamicSpan<double> {
                 assert(dynamic_cast<StateVariables<DisplacementDim> const*>(
                            &state) != nullptr);
                 auto& PlaneWeakness_state =
                     static_cast<StateVariables<DisplacementDim>&>(state);

                 return {&PlaneWeakness_state.eps_p.eff, 1};
             }}};
}

template class PlaneWeakness<2>;
template class PlaneWeakness<3>;

template struct StateVariables<2>;
template struct StateVariables<3>;

template <>
MathLib::KelvinVector::KelvinMatrixType<3> sOdotS<3>(
    MathLib::KelvinVector::KelvinVectorType<3> const& v)
{
    MathLib::KelvinVector::KelvinMatrixType<3> result;

    result(0, 0) = v(0) * v(0);
    result(0, 1) = result(1, 0) = v(3) * v(3) / 2.;
    result(0, 2) = result(2, 0) = v(5) * v(5) / 2.;
    result(0, 3) = result(3, 0) = v(0) * v(3);
    result(0, 4) = result(4, 0) = v(3) * v(5) / std::sqrt(2.);
    result(0, 5) = result(5, 0) = v(0) * v(5);

    result(1, 1) = v(1) * v(1);
    result(1, 2) = result(2, 1) = v(4) * v(4) / 2.;
    result(1, 3) = result(3, 1) = v(3) * v(1);
    result(1, 4) = result(4, 1) = v(1) * v(4);
    result(1, 5) = result(5, 1) = v(3) * v(4) / std::sqrt(2.);

    result(2, 2) = v(2) * v(2);
    result(2, 3) = result(3, 2) = v(5) * v(4) / std::sqrt(2.);
    result(2, 4) = result(4, 2) = v(4) * v(2);
    result(2, 5) = result(5, 2) = v(5) * v(2);

    result(3, 3) = v(0) * v(1) + v(3) * v(3) / 2.;
    result(3, 4) = result(4, 3) =
        v(3) * v(4) / 2. + v(5) * v(1) / std::sqrt(2.);
    result(3, 5) = result(5, 3) =
        v(0) * v(4) / std::sqrt(2.) + v(3) * v(5) / 2.;

    result(4, 4) = v(1) * v(2) + v(4) * v(4) / 2.;
    result(4, 5) = result(5, 4) =
        v(3) * v(2) / std::sqrt(2.) + v(5) * v(4) / 2.;

    result(5, 5) = v(0) * v(2) + v(5) * v(5) / 2.;
    return result;
}

template <>
MathLib::KelvinVector::KelvinMatrixType<2> sOdotS<2>(
    MathLib::KelvinVector::KelvinVectorType<2> const& v)
{
    MathLib::KelvinVector::KelvinMatrixType<2> result;

    result(0, 0) = v(0) * v(0);
    result(0, 1) = result(1, 0) = v(3) * v(3) / 2.;
    result(0, 2) = result(2, 0) = 0;
    result(0, 3) = result(3, 0) = v(0) * v(3);

    result(1, 1) = v(1) * v(1);
    result(1, 2) = result(2, 1) = 0;
    result(1, 3) = result(3, 1) = v(3) * v(1);

    result(2, 2) = v(2) * v(2);
    result(2, 3) = result(3, 2) = 0;

    result(3, 3) = v(0) * v(1) + v(3) * v(3) / 2.;

    return result;
}

}  // namespace PlaneWeakness
}  // namespace Solids
}  // namespace MaterialLib

